<html>
 <head>
 <title>exercise</title>
 </head>
<body>

<?php
        $monthDays = array ('Splorch' => 23, 'Sploo' => 28,
                            'Splat' => 2, 'Splatt' => 3,
                            'Spleen' => 44, 'Splune' => 30,
                            'Spling' => 61, 'Slendo' => 61, 
                            'Sploctember' => 31, 'Splictember' => 31,
                            'Splanet' => 30, 'TheRest' => 22);

        //Q5.b (i)
        echo min($monthDays) . "<br><br>";
        
        //Q5.b (ii)
        //getting shortest month
         $shortM =array_keys($monthDays , min($monthDays));

        //print $shortM array
        //print_r ( $shortM); 
        //echo "<br>";

        // to print the month only. can be used even when there's multiple shortest month
         foreach ($shortM as $value){
             echo $value . "<br><br>";
         }

         //Q5.b (iii)
         echo array_sum ($monthDays);
?>
</body>
</html>
