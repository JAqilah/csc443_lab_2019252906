<html>
<head>
<title>Associative Array</title>
</head>
<body>

<?php
        $monthDays = array ('January' => 31, 'February' => 28,
                            'March' => 31, 'April' => 30,
                            'May' => 31, 'June' => 30,
                            'July' => 31, 'August' => 31,
                            'September' => 30, 'October' => 31,
                            'November' => 30, 'December' => 31);

        //Q4 (a)
        foreach ( $monthDays as $value){
            echo " $value <br>";
        }
        echo "<br><br><br>";
        
         //Q4 (b)
         foreach ( $monthDays as $key => $newVal){
            if ($newVal == 30) 
                echo " $key <br>";
        }               
?>
</body>
</html>